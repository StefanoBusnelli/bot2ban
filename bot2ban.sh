#!/bin/bash

#   Parameters

p_interface=eth0
t_chain=bot2ban
whitelst=""

#   Temporary files

f_ip_pre=/tmp/$t_chain.pre
f_ip_tmp=/tmp/$t_chain.tmp
f_ip_add=/tmp/$t_chain.add

#   Start

echo -e "Checking chains...\n"

if [[ -e /sbin/iptables ]]; then
  echo "iptables is mandatory"
  exit 1
fi

if [[ $(/sbin/iptables -S $t_chain | wc -l) -eq 0 ]];
then
  echo -e "  $t_chain chain not present...\n"
  /sbin/iptables -N $t_chain
  /sbin/iptables -A $t_chain -j RETURN
  /sbin/iptables -I INPUT 2 -i $p_interface -j $t_chain
fi

echo -e "Checking hacks...\n"

if [[ $(/sbin/iptables -S $t_chain | wc -l) -gt 2 ]];
then
  echo -e "  Adding newer hacks...\n"
  startdate=$(date --date="2 hours ago" +"%F %T")
  /sbin/iptables -S $t_chain | egrep -o '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' | sort | uniq > $f_ip_pre
  /bin/journalctl /usr/sbin/sshd --no-hostname -o short-iso -S "$startdate" | grep "Failed password" | egrep -o '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' | egrep -v "$whitelst" | sort | uniq > $f_ip_tmp
else
  echo -e "  Adding all hacks\n"
  echo "" > $f_ip_pre
  /bin/journalctl /usr/sbin/sshd --no-hostname -o short-iso                 | grep "Failed password" | egrep -o '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' | egrep -v "$whitelst" | sort | uniq > $f_ip_tmp
fi

diff /tmp/ban_ip.pre /tmp/ban_ip.tmp | egrep "^>" | cut -c 3- > $f_ip_add
for ip in $(cat $f_ip_add); do
  echo "  Ban ip " $ip
  /sbin/iptables -I $t_chain 1 -s $ip -i $p_interface -j DROP
done

echo -e " " $(cat $f_ip_add | wc -l) "new rules inserted.\n"

echo -e "End\n"

if [[ -e /sbin/iptables-save ]]; then
  /sbin/iptables-save > /etc/iptables/rules.v4
fi;

echo -e "------------------------------------ RULES -------------------------------------\n"

/sbin/iptables -vnL $t_chain
